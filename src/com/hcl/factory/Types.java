package com.hcl.factory;

public class Types {
	public static Classification setmoviedetailsType(int type) {

		if(type==1) {
			return new Upcoming();
		}
		else if(type==2) {
			return new  ReleasedMovies();
		}
		else if(type==3) {
			return new BestMovieOfTheYear();
		}
		else if(type==4) {
			return new TopRatedMovie();
		}
		else {
			return null;
		}
	}

}

