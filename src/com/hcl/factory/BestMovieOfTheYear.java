package com.hcl.factory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import com.hcl.dbc.DataBaseConnection;
import com.hcl.movie.MovieDetails;

public class BestMovieOfTheYear  implements Classification {
	//Connection Establishing
	Connection con =DataBaseConnection.getInstance().getConnection();
	public BestMovieOfTheYear() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public List<MovieDetails> MovieDetailsType() throws SQLException {
		//Displaying movies data 
		List<MovieDetails> movies=new ArrayList<MovieDetails>();
		String sql="select id,title,year,storyline,imdbRating,classification from movie where Classification ='bestmovieoftheyear'";
		Statement statement =con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{

			MovieDetails movie = new MovieDetails();
			movie.setId(rs.getInt(1));
			movie.setTitle(rs.getString(2));
			movie.setYear(rs.getInt(3));
			movie.setStoryline(rs.getString(4));
			movie.setImdbRating(rs.getInt(5));
			movie.setClassification(rs.getString(6));
			movies.add(movie);
		}

		return movies;	
	}


}