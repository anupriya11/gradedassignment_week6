package com.hcl.dbc;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class DataBaseConnection {
	private static DataBaseConnection inst;
 	private static Connection con;
 	private DataBaseConnection() {

 	}
 	public static DataBaseConnection getInstance() {
 		if(inst == null) {
 			inst = new DataBaseConnection();
 		}
 		return inst;
 	}
 	public Connection getConnection() {	
 		if(con == null) {
 			try {
 				Class.forName("com.mysql.cj.jdbc.Driver");
 				con = DriverManager.getConnection(
 						"jdbc:mysql://localhost:3306/movie_W5","root@localhost","YES");
 				System.out.println("driver loaded and connected");
 			} catch (ClassNotFoundException e) {
 				System.out.println(" driver not loaded");
 				e.printStackTrace();
 			} catch (SQLException e) {
 				System.out.println("not connected to database");
 				e.printStackTrace();
 			}
 		}
 		return con;	
 	}

 }



